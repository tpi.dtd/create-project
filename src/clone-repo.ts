import editJsonFile from 'edit-json-file';
import shell from 'shelljs';
import { FRAMEWORK, TEMPLATE, VARIABLES } from './constant';
import { Answer } from './types/answer';

const {
  ADMIN_BRANCH_NAME,
  APP_BRANCH_NAME,
  MONOREPO_VUE_BRANCH_NAME,
  CACHE_DIR,
  TOKEN,
  ANGULAR_REPO,
  VUE_REPO,
  MONOREPO_REPO
} = VARIABLES;

export default function (params: Answer) {
  const { framework, projectName, template, monorepo } = params;
  const repo = getRepo(framework, monorepo);
  const branchName = getBranchName(template,monorepo);

  shell.exec(
    `git clone -b ${branchName} --single-branch ${repo} ${CACHE_DIR}`,
    { silent: true }
  );
  shell.cd(CACHE_DIR);
  shell.rm('-rf', '.git');

  if (projectName) {
    const packageJson = editJsonFile('./package.json');
    packageJson.set('name', projectName);
    packageJson.save();
  }

  shell.cd('..');
  shell.cp('-r', `${CACHE_DIR}/.`, '.');
  shell.rm('-rf', `${CACHE_DIR}`);
}

function getRepo(framework: string, isMonorepo: boolean): string {
  if (isMonorepo === false) {
    const angularRepo = `https://oauth2:${TOKEN}@${ANGULAR_REPO}`;
    const vueRepo = `https://oauth2:${TOKEN}@${VUE_REPO}`;

    return framework === FRAMEWORK.ANGULAR ? angularRepo : vueRepo;
  }

  const monorepoRepo = `https://oauth2:${TOKEN}@${MONOREPO_REPO}`;
  return monorepoRepo
}

function getBranchName(template: string, isMonorepo: boolean): string {
  if (isMonorepo === false) {
    return template === TEMPLATE.APP ? APP_BRANCH_NAME : ADMIN_BRANCH_NAME;
  }

  return MONOREPO_VUE_BRANCH_NAME
}
