export type Answer = {
  'projectName': string;
  'monorepo': boolean;
  'framework': string;
  'template': string;
}
