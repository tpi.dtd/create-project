const FRAMEWORK = {
  ANGULAR: 'Angular',
  VUE: 'Vue',
};

const TEMPLATE = {
  ADMIN: 'Admin',
  APP: 'App',
};

const VARIABLES = {
  APP_BRANCH_NAME: 'app',
  ADMIN_BRANCH_NAME: 'admin',
  MONOREPO_VUE_BRANCH_NAME: 'monorepo-vue',
  CACHE_DIR: 'clone-cache',
  TOKEN: 'glpat-QQSgAyQTswL5aqqfFhZ_',

  ANGULAR_REPO: 'gitlab.com/tpi-dtd/angular-boilerplate.git',
  VUE_REPO: 'gitlab.com/tpi-dtd/vue-boilerplate.git',
  MONOREPO_REPO: 'gitlab.com/tpi-dtd/monorepo-boilerplate.git',
};

export { FRAMEWORK, TEMPLATE, VARIABLES };
