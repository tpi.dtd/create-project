import { green, red } from 'kolorist';
import path from 'path';
import prompts from 'prompts';
import shell from 'shelljs';
import cloneRepo from './clone-repo';
import { FRAMEWORK, TEMPLATE } from './constant';
import { Answer } from './types/answer';

async function init() {
  /** 預設專案名稱 */
  const defaultProjectName = 'tpi-dtd-project';

  /** 是否在當前資料夾建立專案 */
  let buildInCurrentDirectory = false;

  /** 問題列表 */
  const questions: prompts.PromptObject<keyof Answer>[] = [
    // 項目名稱，使用 '.' 時將建立專案於當前所在資料夾
    {
      type: 'text',
      name: 'projectName',
      message:
        'Project name (Enter . will create project in current directory):',
      initial: defaultProjectName,
      format: (value) => {
        return value === '.' ? path.basename(path.resolve()) : value;
      },
      onState: ({ value }) => {
        if (value === '.') {
          buildInCurrentDirectory = true;
        }
      },
    },
    // 是否是 monorepo
    {
      type: 'select',
      name: 'monorepo',
      message: 'Is monorepo?',
      choices: [
        { title: 'No', value: false },
        { title: 'Yes', value: true },
      ],
    },
    // 選擇框架（Angular、Vue）
    {
      type: 'select',
      name: 'framework',
      message: 'Select a framework:',
      choices: (prev) => {
        const vueChoice = { title: green(FRAMEWORK.VUE), value: FRAMEWORK.VUE }
        const angularChoice = {
          title: red(FRAMEWORK.ANGULAR),
          value: FRAMEWORK.ANGULAR,
        }

        if(prev){
          return [vueChoice]
        }
        return [
          vueChoice, angularChoice
        ]
      },
    },
    // 選擇模板（App、Admin）
    {
      type: (_, { monorepo }) => {
        return monorepo ? null : 'select';
      },
      name: 'template',
      message: 'Please select template',
      choices: [
        { title: TEMPLATE.APP, value: TEMPLATE.APP },
        { title: TEMPLATE.ADMIN, value: TEMPLATE.ADMIN },
      ],
    },
  ];

  const response: Answer = await prompts(questions);
  const { projectName } = response;

  // 如果專案名稱不為 '.' 時，建立新的資料夾
  if (!buildInCurrentDirectory) {
    shell.mkdir(projectName);
    shell.cd(projectName);
  }

  console.log(`\nScaffolding project in ${path.join(process.cwd())}...`);

  // 依照選項 clone 相對應的專案架構
  cloneRepo(response);

  console.log(`\nDone. Now run:\n`);

  // 如果有新增資料夾時，回到原本的資料夾
  if (!buildInCurrentDirectory) {
    shell.cd('..');
    console.log(`  cd ${projectName}`);
  }

  console.log('  pnpm install');
  console.log('  pnpm run dev\n');
}

init().catch((error) => {
  console.error(error);
});
