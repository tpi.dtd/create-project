"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const kolorist_1 = require("kolorist");
const path_1 = __importDefault(require("path"));
const prompts_1 = __importDefault(require("prompts"));
const shelljs_1 = __importDefault(require("shelljs"));
const clone_repo_1 = __importDefault(require("./clone-repo"));
const constant_1 = require("./constant");
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        /** 預設專案名稱 */
        const defaultProjectName = 'tpi-dtd-project';
        /** 是否在當前資料夾建立專案 */
        let buildInCurrentDirectory = false;
        /** 問題列表 */
        const questions = [
            // 項目名稱，使用 '.' 時將建立專案於當前所在資料夾
            {
                type: 'text',
                name: 'projectName',
                message: 'Project name (Enter . will create project in current directory):',
                initial: defaultProjectName,
                format: (value) => {
                    return value === '.' ? path_1.default.basename(path_1.default.resolve()) : value;
                },
                onState: ({ value }) => {
                    if (value === '.') {
                        buildInCurrentDirectory = true;
                    }
                },
            },
            // 是否是 monorepo
            {
                type: 'select',
                name: 'monorepo',
                message: 'Is monorepo?',
                choices: [
                    { title: 'No', value: false },
                    { title: 'Yes', value: true },
                ],
            },
            // 選擇框架（Angular、Vue）
            {
                type: 'select',
                name: 'framework',
                message: 'Select a framework:',
                choices: (prev) => {
                    const vueChoice = { title: (0, kolorist_1.green)(constant_1.FRAMEWORK.VUE), value: constant_1.FRAMEWORK.VUE };
                    const angularChoice = {
                        title: (0, kolorist_1.red)(constant_1.FRAMEWORK.ANGULAR),
                        value: constant_1.FRAMEWORK.ANGULAR,
                    };
                    if (prev) {
                        return [vueChoice];
                    }
                    return [
                        vueChoice, angularChoice
                    ];
                },
            },
            // 選擇模板（App、Admin）
            {
                type: (_, { monorepo }) => {
                    return monorepo ? null : 'select';
                },
                name: 'template',
                message: 'Please select template',
                choices: [
                    { title: constant_1.TEMPLATE.APP, value: constant_1.TEMPLATE.APP },
                    { title: constant_1.TEMPLATE.ADMIN, value: constant_1.TEMPLATE.ADMIN },
                ],
            },
        ];
        const response = yield (0, prompts_1.default)(questions);
        const { projectName } = response;
        // 如果專案名稱不為 '.' 時，建立新的資料夾
        if (!buildInCurrentDirectory) {
            shelljs_1.default.mkdir(projectName);
            shelljs_1.default.cd(projectName);
        }
        console.log(`\nScaffolding project in ${path_1.default.join(process.cwd())}...`);
        // 依照選項 clone 相對應的專案架構
        (0, clone_repo_1.default)(response);
        console.log(`\nDone. Now run:\n`);
        // 如果有新增資料夾時，回到原本的資料夾
        if (!buildInCurrentDirectory) {
            shelljs_1.default.cd('..');
            console.log(`  cd ${projectName}`);
        }
        console.log('  pnpm install');
        console.log('  pnpm run dev\n');
    });
}
init().catch((error) => {
    console.error(error);
});
//# sourceMappingURL=index.js.map