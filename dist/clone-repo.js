"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const edit_json_file_1 = __importDefault(require("edit-json-file"));
const shelljs_1 = __importDefault(require("shelljs"));
const constant_1 = require("./constant");
const { ADMIN_BRANCH_NAME, APP_BRANCH_NAME, MONOREPO_VUE_BRANCH_NAME, CACHE_DIR, TOKEN, ANGULAR_REPO, VUE_REPO, MONOREPO_REPO } = constant_1.VARIABLES;
function default_1(params) {
    const { framework, projectName, template, monorepo } = params;
    const repo = getRepo(framework, monorepo);
    const branchName = getBranchName(template, monorepo);
    shelljs_1.default.exec(`git clone -b ${branchName} --single-branch ${repo} ${CACHE_DIR}`, { silent: true });
    shelljs_1.default.cd(CACHE_DIR);
    shelljs_1.default.rm('-rf', '.git');
    if (projectName) {
        const packageJson = (0, edit_json_file_1.default)('./package.json');
        packageJson.set('name', projectName);
        packageJson.save();
    }
    shelljs_1.default.cd('..');
    shelljs_1.default.cp('-r', `${CACHE_DIR}/.`, '.');
    shelljs_1.default.rm('-rf', `${CACHE_DIR}`);
}
exports.default = default_1;
function getRepo(framework, isMonorepo) {
    if (isMonorepo === false) {
        const angularRepo = `https://oauth2:${TOKEN}@${ANGULAR_REPO}`;
        const vueRepo = `https://oauth2:${TOKEN}@${VUE_REPO}`;
        return framework === constant_1.FRAMEWORK.ANGULAR ? angularRepo : vueRepo;
    }
    const monorepoRepo = `https://oauth2:${TOKEN}@${MONOREPO_REPO}`;
    return monorepoRepo;
}
function getBranchName(template, isMonorepo) {
    if (isMonorepo === false) {
        return template === constant_1.TEMPLATE.APP ? APP_BRANCH_NAME : ADMIN_BRANCH_NAME;
    }
    return MONOREPO_VUE_BRANCH_NAME;
}
//# sourceMappingURL=clone-repo.js.map